module andOr (a,b,c,f1,f2);
input a,b,c;
output f1,f2;
wire ab,ab2,bc,na;
not #(10) (na,a);
and #(10) (ab,a,b);
and #(10) (ab2,na,b);
and #(10) (bc,b,c);
or  #(10) (f1,ab,ab2,bc);
or  #(10) (f2,ab,ab2);
endmodule