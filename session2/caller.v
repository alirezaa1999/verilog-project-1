module caller;
reg  a,b,c;
wire f1,f2;
//wire t1,t2,t3,t4;
andOr ao(a,b,c,f1,f2);
initial
begin
a=1;
b=1;
c=1;

#(100) a=0;

#(100) a=1;

#(100) c=1;
#(100) b=1;
end
endmodule