module andOr (a,b,c,f1);
input a,b,c;
output f1;
wire ab,ab2,bc,na;
not #(1) (na,a);
and #(2) (ab,a,b);
and #(2) (ab2,na,b);
and #(2) (bc,b,c);
or  #(3) (f1,ab,ab2,bc);
endmodule