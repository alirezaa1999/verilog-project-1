    module RAM_64bits ( clk, read, write, enable, addr_of_word_to_be_read ,addr_of_word_to_be_written, output_destination, word_to_be_written)
     
    //parameters
     
    parameter words_no= 32; //arbitrary, we need 5 bits to address them
    parameter bus_width=64;
    parameter address_width=5;
     
    //ports
     
    input  clk;
    input  read;
    input  write;
    input  enable;
    input  word_to_be_written;
    input  addr_of_word_to_be_read;
    input  addr_of_word_to_be_written;
    output output_destination;
     
    //ports type and width
     
    wire                 clk;
    wire                 enable;
    wire                 read; 
    wire                 write; 
    wire [address_width-1:0]           addr_of_word_to_be_read;
    wire [address_width-1:0]           addr_of_word_to_be_written;
    wire [bus_width-1:0] word_to_be_written;
    reg  [bus_width-1:0] output_destination; 
     
    //memory declaration
     
    reg [bus_width-1:0] mem [0:words_no-1];//memory of 32 words,each is 64bit
     
    // behavior
     
     always@(posedge clk)
      begin 
         if (enable==1'b1)
          begin
     
    		 if (read==1'b1) 
    		   begin
     
    			 // to read a specific word, you need to address its number. 
                 output_destination<=mem [addr_of_word_to_be_read];
    		   end
     
    			 if (write==1'b1)
    			 mem[addr_of_word_to_be_written]<=word_to_be_written;
          end
      end
     
    endmodule