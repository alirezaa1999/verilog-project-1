
module arithimetic_unit_64(a, b, c, op, enable, carry_in, carry_out, au_clk);
	input [63:0] a,b;// 1st signal
   input au_clk;// clk signal
	input carry_in;
   input [2:0] op;// opcode operation to be performed
	input enable;
	output [127:0] c;// output signal
	output carry_out;
	
	reg [127:0] c;
	
								

		always @ (posedge au_clk)
					begin
						case(opcode)
								//addition
								6'd0:addition64 (a, b, c, carry_in);
								//subtraction
								6'd1:subtraction64 (a, b, c, carry_in);
								//addition with carry
								6'd2:addition64 (a, b, c, carry_in);
								//subtraction with borrow
								6'd3:subtraction64 (a, b, c, carry_in);
								//multiplication
								6'd4:multiplier64 (a, b, c);
								//and
								6'd5:and64 (1'b1, c, a, b, 0, clk);
								//or
								6'd6:or64 (1'b1, c, a, b, 0, clk);
							endcase
					end
endmodule

task subtraction64;
input [63:0]a,b;
input carry_in;
output [63:0] c;

begin
{carry_out,c[63:0]}=a-b-carry_in;
end
endtask



task multiplier64;
	input [63:0]a,b;
	output [63:0]c;
	

	c = a*b;
endtask



task and64;
	input [63:0]a,b;
	output [63:0]c;
	

	c=a&b;
endtask


task addition64;
	input [63:0]a,b;
	input carry_in;
	output [63:0]c;
	
	begin
	{carry_out,c[63:0]}=a+b+carry_in;
	
end
endtask

task or64;
	input [63:0]a;
	input [63:0]b;
	output [63:0]c;
	
	c = a|b;
	
endtask