module caller;
reg  a,b,c;
wire f1;
//wire t1,t2,t3,t4;
andOr ao(a,b,c,f1);
initial
begin
a=0;
b=0;
c=0;
end
always 
begin
a=1;
b=1;
c=1;
end
endmodule